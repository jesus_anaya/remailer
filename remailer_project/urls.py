from django.conf.urls import patterns, include, url
from django.contrib import admin
import backbone

# Uncomment the next two lines to enable the admin:
admin.autodiscover()
backbone.autodiscover()


urlpatterns = patterns('',
    url(r'^', include('remailer.urls')),
    url(r'^', include(admin.site.urls)),
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^backbone/', include(backbone.site.urls)),
    url(r'^chronograph/job/(?P<pk>\d+)/run/$', 'chronograph.views.job_run', name="admin_chronograph_job_run")
)
