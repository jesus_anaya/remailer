jQuery(function($) {

    $('#id_date_data').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm-dd-yy'
    });

    $('#id_contact_date').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm-dd-yy'
    });

    $("#id_birth_date").datepicker({
        dateFormat : 'yy-mm-dd',
        changeMonth : true,
        changeYear : true,
        yearRange: '-100y:c+nn'
    });

    $('#id_time_data').timepicker({showPeriod: true, showLeadingZero: true});

    $('.date_data').datepicker();
    $('.date_data').datepicker('option', {dateFormat: 'dd-mm-yy'});

    $("#ui-datepicker-div").css({'visibility': 'hidden', 'display': 'none'});

    $(".hasDatepicker").on('click', function(){
        $("#ui-datepicker-div").css({'visibility': 'visible', 'display': 'inline'});
    });
});