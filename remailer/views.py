# Create your views here.

from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User
from django.core import serializers
from django.utils import simplejson as json
from models import Patient, Promoter
from datetime import datetime
import unicodecsv


@login_required(login_url="/login/")
def patient_validation(request):
    first_name = request.GET.get('first_name')
    last_name = request.GET.get('last_name')
    email = request.GET.get('email')

    if email:
        patients = Patient.objects.filter(
            Q(first_name=first_name, last_name=last_name) | Q(email=email))
    else:
        patients = Patient.objects.filter(first_name=first_name, last_name=last_name)

    if patients.exists():
        plist = json.loads(serializers.serialize('json', patients, use_natural_keys=True))
        data = {'exists': True, 'plist': plist, 'results': len(plist)}
    else:
        data = {'exists': False}

    return HttpResponse(json.dumps(data), 'application/json')


@login_required(login_url="/login/")
def delete_patient(request, id):
    try:
        patient = Patient.objects.get(id=id)
        patient.delete()
    except:
        pass
    return HttpResponseRedirect("/remailer/patient/add/#list")


@login_required(login_url="/login/")
def change_date_patient(request, id):
    try:
        patient = Patient.objects.get(id=id)
        patient.date = not patient.date
        if not patient.date:
            patient.state = 2
        elif patient.date:
            patient.state = 1

        patient.save()
    except:
        pass
    return HttpResponseRedirect("/remailer/patient/add/#list")


@login_required(login_url="/login/")
def change_to_no_show(request, id):
    try:
        patient = Patient.objects.get(id=id)
        patient.change_to_no_show()
    except:
        pass
    return HttpResponseRedirect("/remailer/patient/add/#list")


@login_required(login_url="/login/")
def home(request):
    return render_to_response('home.html', context_instance=RequestContext(request))


@login_required(login_url="/login/")
def export(request):
    return render_to_response(
        'export.html', {
            'promoters': Promoter.objects.all(),
            'users': User.objects.all(),
        }, context_instance=RequestContext(request))


def get_scv(request):
    titles = ["Contacto", "Fecha", "Hora"]
    patients = Patient.objects.all()

    if(request.method == 'POST'):
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        phone = request.POST.get('phone', '')
        movil = request.POST.get('movil', '')
        clinica = request.POST.get('clinica', '')
        trata = request.POST.get('trata', '')
        comments = request.POST.get('comments', '')
        reward_program = request.POST.get('reward_program')
        typeof = request.POST.get('type', '')
        user = request.POST.get('user', '')
        promo = request.POST.get('promo', '')
        date_init = request.POST.get('date_init', '')
        date_end = request.POST.get('date_end', '')
        birth_date = request.POST.get('birth_date')
        city_of_residence = request.POST.get('city_of_residence')
        promo_code = request.POST.get('promo_code')
        procedures = request.POST.get('procedures')

    if typeof != '':
        patients = patients.filter(state=int(typeof))

    if date_init != '' and date_end != '':
        rdate_init = datetime.strptime(date_init, '%d-%m-%Y')
        rdate_end = datetime.strptime(date_end, '%d-%m-%Y')
        patients = patients.filter(contact_date__gte=rdate_init, contact_date__lte=rdate_end)

    if user != '':
        patients = patients.filter(user__id=user)
    if promo != '':
        patients = patients.filter(promoter__id=promo)

    if name != '':
        titles += ["Nombre", "Apellido"]
    if email != '':
        titles += ["Email"]
    if phone != '':
        titles += ["Telefono"]
    if movil != '':
        titles += ["Tel. Celular"]
    if clinica != '':
        titles += ["Clinica"]
    if trata != '':
        titles += ["Appointment Status"]

    titles += ["Active Date"]

    if procedures:
        titles += ["Procedures"]
    if comments != '':
        titles += ["Comentario"]
    if reward_program:
        titles += ["Reward Program Names"]
    if birth_date:
        titles += ["Birth Date"]
    if city_of_residence:
        titles += ["City of Residence"]
    if promo_code:
        titles += ["Promo Code"]

    titles += ["Promotor", "Usuario"]

    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment;filename="patients-%s.csv"' %\
        datetime.now().strftime("%Y/%m/%d-%H:%M:%S")
    writer = unicodecsv.writer(response, encoding='utf-8')
    writer.writerow(titles)

    STATE = ["Patient prospect", "Patient", "Canceled", "No Show"]

    for patient in patients:
        fields = [patient.contact_date, unicode(patient.date_data), unicode(patient.time_data)]
        if name != '':
            fields += [patient.first_name, patient.last_name]
        if email != '':
            fields += [patient.email]
        if phone != '':
            fields += [patient.telephone]
        if movil != '':
            fields += [patient.mobile]
        if clinica != '':
            fields += [patient.clinic]
        if trata != '':
            fields += [patient.treatment]

        fields += [STATE[int(patient.state)]]

        if procedures:
            fields += [patient.procedures]
        if comments != '':
            fields += [patient.comment]
        if reward_program:
            fields += [patient.reward_name]
        if birth_date:
            fields += [patient.birth_date]
        if city_of_residence:
            fields += [patient.city_of_residence]
        if promo_code:
            fields += [patient.promo_code]

        fields += [patient.promoter, patient.user]

        writer.writerow(fields)

    return response
