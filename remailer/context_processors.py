from django.db.models import Q
from models import Patient
from helpers import paginator


def patient_list(request):

    patients = Patient.objects.all()

    search = request.GET.get('search')
    if search:
        patients = patients.filter(Q(search_info__icontains=search))

    filter_order = request.GET.get('filter_order')
    filter_by = request.GET.get('filter', 'contact_date')
    filter_order_m = request.GET.get('filter_order_m')

    if not filter_order and not filter_order_m:
        filter_order = 'desc'
        filter_icon = "icon-chevron-down"
    else:
        if filter_order_m:
            filter_order = 'desc' if filter_order_m == 'asc' else 'asc'
        filter_icon = "icon-chevron-down" if filter_order == 'desc' else "icon-chevron-up"

    patients = patients.order_by(('' if filter_order == 'asc' else '-') + filter_by)
    patients_paginations = paginator(request, patients, 30)

    return {
        'patients': patients_paginations,
        'filter': filter_by,
        'filter_icon': filter_icon,
        'filter_order': filter_order,
        'search': search,
    }
