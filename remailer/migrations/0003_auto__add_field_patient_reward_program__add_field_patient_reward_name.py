# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Patient.reward_program'
        db.add_column(u'remailer_patient', 'reward_program',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Patient.reward_name'
        db.add_column(u'remailer_patient', 'reward_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=120),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Patient.reward_program'
        db.delete_column(u'remailer_patient', 'reward_program')

        # Deleting field 'Patient.reward_name'
        db.delete_column(u'remailer_patient', 'reward_name')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'remailer.bccmail': {
            'Meta': {'object_name': 'BCCMail'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remailer.Clinic']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'remailer.clinic': {
            'Meta': {'object_name': 'Clinic'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email_confirm': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'email_confirm_password': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email_confirm_port': ('django.db.models.fields.IntegerField', [], {'default': '465'}),
            'email_confirm_server': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email_feedback': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'email_feedback_password': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email_feedback_port': ('django.db.models.fields.IntegerField', [], {'default': '465'}),
            'email_feedback_server': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email_remember': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'email_remember_password': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email_remember_port': ('django.db.models.fields.IntegerField', [], {'default': '465'}),
            'email_remember_server': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email_sanico': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'email_sanico_password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'email_sanico_port': ('django.db.models.fields.IntegerField', [], {'default': '465'}),
            'email_sanico_server': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        'remailer.email': {
            'Meta': {'ordering': "['-id']", 'object_name': 'Email'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remailer.Clinic']"}),
            'emaile_type': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'remailer.patient': {
            'Meta': {'ordering': "['-contact_date', '-id']", 'object_name': 'Patient'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remailer.Clinic']"}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'contact_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'date': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_data': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email_phase': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'modify_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'promoter': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remailer.Promoter']", 'null': 'True', 'blank': 'True'}),
            'reward_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '120'}),
            'reward_program': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'search_info': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'time_data': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'treatment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        'remailer.promoter': {
            'Meta': {'object_name': 'Promoter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['remailer']