"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import Client, TestCase, RequestFactory
from django.utils import simplejson as json
from django.middleware.csrf import get_token
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from remailer.backbone_api import PatientAPIView
from models import Clinic, Promoter


class SimpleTest(TestCase):
    def setUp(self):
        self.client = Client(enforce_csrf_checks=False)

        self.user = User.objects.create_user('anaya', 'j@moz.com', '1234')

    def create_clinic(self):
        clinic = Clinic()
        clinic.id = 1
        clinic.name = 'Clinic Test'
        clinic.address = 'Address 1'
        clinic.telephone = '5555'

        clinic.email_confirm = 'a@anaya.com'
        clinic.email_confirm_password = '1234'
        clinic.email_confirm_server = 'mail.anaya.com'
        clinic.email_confirm_port = 25

        clinic.email_remember = 'a@anaya.com'
        clinic.email_remember_password = '1234'
        clinic.email_remember_server = 'mail.anaya.com'
        clinic.email_remember_port = 25

        clinic.email_feedback = 'a@anaya.com'
        clinic.email_feedback_password = '1234'
        clinic.email_feedback_server = 'mail.anaya.com'
        clinic.email_feedback_port = 25
        clinic.save()
        return clinic

    def create_promoter(self):
        promoter = Promoter()
        promoter.id = 1
        promoter.name = "anaya"
        promoter.save()
        return promoter

    def login(self):
        assert self.client.login(username='anaya', password='1234')

    def test_send_patient(self):
        """
        Test: Creatin patient
        """

        #self.login()

        response = self.client.get('/', follow=True)
        token = response._request.META['CSRF_COOKIE']
        headers = {
            'X-CSRFToken': token
        }

        print "token: ", token

        url = reverse('backbone:remailer_patient')
        print url

        clinic = self.create_clinic()
        promoter = self.create_promoter()
        data = json.dumps({
                "clinic": clinic.id,
                "comment": "Comentario1",
                "date": False,
                "date_data": "2013-02-01",
                "time_data": "18:43:00",
                "email": "killeranaya@gmail.com",
                "first_name": "Jesus",
                "last_name": "Anaya Orozco",
                "mobile": "686 2154052",
                "promoter": promoter.id,
                "telephone": "686 2154052",
                "treatment": "tratamiento",
                "user": self.user.id
            })

        response = self.client.post(url, data, 'application/json',
            **headers)

        print response._request
        self.assertEqual(response.status_code, 201)
