$(document).on('ready', function(){
    $("input").on("click", verify_all_checked);
    $("input").on("change", verify_all_checked);
    $("select").on("change", verify_all_checked);
});

function verify_all_checked()
{
    if($('#check-all').is(':checked'))
    {
        $("input[name=name]").attr('checked', true);
        $("input[name=email]").attr('checked', true);
        $("input[name=phone]").attr('checked', true);
        $("input[name=movil]").attr('checked', true);
        $("input[name=clinica]").attr('checked', true);
        $("input[name=trata]").attr('checked', true);
        $("input[name=procedures]").attr('checked', true);
        $("input[name=birth_date]").attr('checked', true);
        $("input[name=city_of_residence]").attr('checked', true);
        $("input[name=promo_code]").attr('checked', true);
        $("input[name=comments]").attr('checked', true);
        $("input[name=reward_program]").attr('checked', true);
        $("select[name=promo]").val('');
        $("select[name=type]").val('');
        $("select[name=user]").val('');
        $("input[name=date_end]").val('');
        $("input[name=date_init]").val('');
    }
}