// patient_script.js
// by Jesus Anaya
//

$(document).on('ready', function(){
    /*
     * Models
     */
    var Patient = Backbone.Model.extend({
        urlRoot: $("meta[name='patient-model']").attr('content')
    });

    /*
     * Collections
     */
    window.PatientCollection = Backbone.Collection.extend({
        model: Patient,
        url: $("meta[name='patient-model']").attr('content'),

        initialize: function(){
            this.order_by('contact_date_format', 'asc');
        },

        comparator: function(patient){
            var data = patient.get(this._sort_by);
            if(typeof data == 'string')
            {
                var str_array = data.split("");
                var res = 0;
                for(var x = 9, i = 0; i < str_array.length; i++, x--){
                    res += str_array[i].charCodeAt() * Math.pow(10, x);
                }
                if(this._sort_order == 'asc'){
                    return -res;
                } else {
                    return res;
                }
            } else {
                if(this._sort_order == 'asc')
                    return -data;
                else if(this._sort_order == 'desc')
                    return data;
                return -patient.id;
            }
        },

        search: function(letters) {
            if(letters === "")
                return this;

            var pattern = new RegExp(letters, "mi");
            return _(this.filter(function(data) {
                return pattern.test(data.get("patient_info"));
            }));
        },

        // order_by can be asc and desc
        order_by: function(by, in_order){
            this._sort_order = in_order;
            this._sort_by = by;
            this.sort();
            this.trigger('reset');
        }
    });

    // Views
    window.PatientListView = Backbone.View.extend({
        initialize: function() {
            this.collection.bind("reset", this.render, this);
            this.collection.bind("change", this.render, this);
            this.collection.bind("add", this.render, this);
            this.from_user = -1;
        },

        renderList: function(patients){
            /*
            var patients_list;
            {
                patients_list = this.collection.models;
            } else {
                patients_list = this.collection.where({user: this.from_user});
            }
            */
            $(this.el).empty();
            var self = $(this.el);
            var intance = this;
            patients.each(function (patient){
                if(intance.from_user === -1 || patient.get('user') === intance.from_user) {
                    if (patient !== undefined) {
                        self.append(new PatientListItemView({
                            model: patient
                        }).render().el);
                    }
                }
            }, this);

            // add comment Pop Over bootstap twitter effect
            $('.comment-patient').popover({
                placement: 'bottom',
                animation: true,
                trigger: 'click',
                offset: 10
            });

            return this;
        },

        render: function(eventName){
            this.renderList(this.collection);
            return this;
        }
    });

    window.PatientListItemView = Backbone.View.extend({
        tagName: "tr",
        className: "row-patient",
        template: Mustache.template('patients'),

        events: {
            "click .delete": "deletePatient",
            "click .cancel": "cancelPatient",
            "click .reactive": "reactivePatient"
        },

        initialize: function () {
            this.model.bind("change", this.render, this);
        },

        render: function (eventName) {
            $(this.el).html(this.template.render({
                'patient': this.model.toJSON(),
                'is_current_user': this.is_current_user(),
                'limitLength': function() {
                    return function(text, render) {
                        var line = render(text); //.substr(0, 4);
                        return (line.length > 10 ? line.substr(0, 10) + '...': line);
                    };
                }
            }));
            return this;
        },

        is_current_user: function(){
            if($("meta[name='user-root']").attr('content') === 'yes'){
                return true;
            } else {
                var id = parseInt($('meta[name="user-id"]').attr('content'), 10);
                if(id == this.model.get('user')){
                    return true;
                }
            }
            return false;
        },

        close: function () {
            $(this.el).unbind();
            $(this.el).remove();
        },

        deletePatient: function(){
            if(confirm("Are you sure you want to delete the record?")){
                this.model.destroy();
                $(this.el).remove();
            }
        },

        cancelPatient: function(){
            if(confirm("Are you sure you want to cancel the appointment?")){
                this.model.set("date", false);
                this.model.save();
            }
        },

        reactivePatient: function(){
            if(confirm("Are you sure you want to activate the appointment?")){
                this.model.set("date", true);
                this.model.save();
            }
        }
    });

    // Router
    var AppRouter = Backbone.Router.extend({
        routes:{
            "!/edit/:id": "editForm"
        },

        initialize: function(){
            this.patient_collection = new PatientCollection();
            this.model = new Patient();


            this.patient_list_view = new PatientListView({
                collection: this.patient_collection,
                el: $('#patients-list')
            });

            this.userUserOnly();
            this.patient_collection.fetch();
            this.patient_list_view.render();

            $("#form-new").on("click", $.proxy(this.newForm, this));
            $("#form-save").on("click", $.proxy(this.saveForm, this));
            $('#form-save-as-new').on("click", $.proxy(this.saveAsNew, this));
            $("#form-search-text").on("keyup", $.proxy(this.search, this));
            $("#form-search-clear").on("click", $.proxy(this.clearSearch, this));
            $("#use-only-user").on("click", $.proxy(this.userUserOnly, this));

            $(".sortable").bind("click", {self: this}, function(event){
                event.data.self.orderList($(this));
            });
        },

        userUserOnly: function(){
            if($("#use-only-user").is(':checked')){
                var id = parseInt($('meta[name="user-id"]').attr('content'), 10);
                this.patient_list_view.from_user = id;
            } else {
                this.patient_list_view.from_user = -1;
            }
            this.patient_collection.trigger('reset');
        },

        orderList: function(filter){
            var arrow = filter.find("i");
            var order = arrow.attr("data-sort");

            if(arrow.attr("class") == "icon-chevron-down"){
                arrow.attr("class", "icon-chevron-up");
                this.patient_collection.order_by(order, 'asc');
            }else if(arrow.attr("class") == "icon-chevron-up"){
                arrow.attr("class", "icon-chevron-down");
                this.patient_collection.order_by(order, 'desc');
            }
        },

        clearUrl: function(){
            //this.patient_collection.fetch();
            setTimeout(function(){
                window.location.href = window.location.href.split("#")[0] + "#!";
            }, 200);
        },

        clearSearch: function(){
            $("#form-search-text").val("");
            this.patient_list_view.renderList(this.patient_list_view.collection);
            this.patient_collection.trigger('reset');
        },

        search: function(){
            if($("#form-search-text").val().length > 3){
                this.patient_list_view.renderList(this.patient_collection.search($("#form-search-text").val()));
            }
            if($("#form-search-text").val().length === 0) {
                this.clearSearch();
            }
        },

        showSaveAsNew: function(){
            $('#form-save-as-new').css({
                "visibility": "visible",
                "display": "inline"
            });
        },

        hideSaveAsNew: function(){
            $('#form-save-as-new').css({
                "visibility": "hidden",
                "display": "none"
            });
        },

        resetForm: function(){
            $('#patient_form').each(function(){
                this.reset();
            });
            $('#id_first_name').focus();
            this.clearUrl();
        },

        newForm: function(){
            this.resetForm();
            this.hideSaveAsNew();

            if(!this.model.isNew()){
                this.model = new Patient();
            }
        },

        editForm: function(id){
            try {
                this.showSaveAsNew();
                this.model = this.patient_collection.get(id);

                $('#id_clinic').val(this.model.get('clinic'));
                $('#id_comment').val(this.model.get('comment'));
                $('#id_date').attr('checked', this.model.get('date'));
                $('#id_contact_date').val(this.model.get('contact_date_format'));
                $('#id_date_data').val(this.model.get('date_converted'));
                $('#id_time_data').val(this.model.get('time_data'));
                $('#id_email').val(this.model.get('email'));
                $('#id_first_name').val(this.model.get('first_name'));
                $('#id_last_name').val(this.model.get('last_name'));
                $('#id_mobile').val(this.model.get('mobile'));
                $('#id_promoter').val(this.model.get('promoter'));
                $('#id_telephone').val(this.model.get('telephone'));
                $('#id_treatment').val(this.model.get('treatment'));
            } catch(err) {
                try {
                    this.clearUrl();
                } catch(err2) {
                    console.log("error updating model");
                }
            }
        },

        saveForm: function(){
            if($("#patient_form").valid()){
                this.hideSaveAsNew();
                this.model.set({
                    clinic: parseInt($('#id_clinic').val(), 10),
                    comment: $('#id_comment').val(),
                    date: ($('#id_date').is(':checked') ? true : false),
                    contact_date: $('#id_contact_date').val(),
                    date_data: $('#id_date_data').val(),
                    time_data: $("#id_time_data").val(),
                    email: $('#id_email').val(),
                    first_name: $('#id_first_name').val(),
                    last_name: $('#id_last_name').val(),
                    mobile: $('#id_mobile').val(),
                    promoter: parseInt($('#id_promoter').val(), 10),
                    telephone: $('#id_telephone').val(),
                    treatment: $('#id_treatment').val(),
                    user: parseInt($('meta[name="user-id"]').attr('content'), 10)
                });
                if(this.model.isNew()){
                    this.patient_collection.create(this.model);
                }else{
                    this.model.save();
                }
                this.newForm();

            }
            this.clearUrl();
        },

        saveAsNew: function(){
            this.model = new Patient();
            this.saveForm();
        }
    });

    var app = new AppRouter();
    Backbone.history.start();
});