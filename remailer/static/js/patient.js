function PatientsController($scope, $http) {
    $scope.url = "/patient-validation/";
    $scope.patients = [];

    if($("p.alert.alert-error").text().toString().search("errors") === -1) {
        if(window.location.href.indexOf('add') != -1) {
            $scope.user_valid = false;
        } else {
            $scope.user_valid = true;
        }
    } else {
        $scope.user_valid = true;
    }

    $scope.validate_patient = function() {
        if(!$scope.first_name || !$scope.last_name) {
            alert("first_name and last_name are needed!");
        } else {
            var url = $scope.url + '?first_name=' + $scope.first_name + '&last_name=' + $scope.last_name;

            if($scope.email)
                url += '&email=' + $scope.email;

            $http.get(url).success(function(data){
                if(data.exists) {
                    if(confirm("The patient already exists, do you want to load this patient information!")) {
                        if(data.results > 1) {
                            $scope.patients = data.plist;
                            $('#modal-choice-patients').modal('show');
                        } else {
                            $scope.open_patient(data.plist[0].pk);
                        }
                    } else {
                        $scope.new_user();
                        $scope.user_valid = true;
                    }
                } else {
                    $scope.new_user();
                    $scope.user_valid = true;
                }
            });
        }
    };

    $scope.new_user = function() {
        $("#id_first_name").val($scope.first_name);
        $("#id_last_name").val($scope.last_name);
        $("#id_email").val($scope.email);
    };

    $scope.open_patient = function(pk) {
        window.location.href = "/remailer/patient/" + pk + "/";
    };
}

$(document).on('ready', function(){
    if(window.location.href.indexOf('add') === -1){
        $("#form-save-as-new").css({
            'visibility': 'visible',
            'display': 'inline'
        });

        $("#form-udate-appointment").css({
            'visibility': 'visible',
            'display': 'inline'
        });

        $("#form-save").val('Update Patient');
    }

    $('.comment-patient').popover({
        placement: 'bottom',
        animation: true,
        trigger: 'click',
        offset: 10
    });

    $("#patient_form").submit(function(e) {
        read_prosedures();
        this.submit();
    });

    //
    $('#procedures').dropdownchecklist({ width: 200 });

    //
    write_procedures();

});

function read_prosedures(){
    var str = '';
    var procedures = $('#id_procedures').val().split(', ');
    $('.ui-dropdownchecklist-item').find('input[type="checkbox"]').each(function(){
        if($(this).is(':checked')){
            str += $(this).parent().find('label').text() + ', ';
        }
    });
    $('#id_procedures').val(str);
}

function is_in(str, procedures) {
    return procedures.indexOf(str) !== -1;
}

function write_procedures(){
    var procedures = $('#id_procedures').val().split(', ');
    $('.ui-dropdownchecklist-item').find('input[type="checkbox"]').each(function(){
        if(is_in($(this).parent().find('label').text(), procedures)) {
            $(this).attr('checked', true);
        }
    });
}

function saveAsNew(){
    $('#patient_form').attr('action', '/remailer/patient/add/');
}

function delete_patient(id){
    if(confirm("Are you sure you want to delete the record?")){
        window.location.href = '/delete_patient/' + id + '/';
    }
}

function change_to_no_show(id){
    if(confirm("Are you sure you want change to 'No Show' this patient?")){
        window.location.href = '/change_to_no_show/' + id + '/';
    }
}

function change_patient_date(id, type){
    switch(type){
        case 'activate':
            if(confirm("Are you sure you want to activate the appointment?")){
                window.location.href = '/change_date_patient/' + id + '/';
            }
            break;

        case 'cancel':
            if(confirm("Are you sure you want to cancel the appointment?")){
                window.location.href = '/change_date_patient/' + id + '/';
            }
            break;
    }
}