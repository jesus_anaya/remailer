//email_script.js

$(document).on('ready', function(){
    $("#id_body").kendoEditor({
        encoded: false,
        messages: {
            insertHtml: "Insert Tags"
        },
        tools: [
         "bold",
         "italic",
         "underline",
         "separator",
         "strikethrough",
         "fontName",
         "fontSize",
         "foreColor",
         "backColor",
         "justifyLeft",
         "justifyCenter",
         "justifyRight",
         "justifyFull",
         "insertUnorderedList",
         "insertOrderedList",
         "indent",
         "outdent",
         "formatBlock",
         "createLink",
         "unlink",
         "insertImage",
         "insertHtml",
         "viewHtml"
     ],
     insertHtml: [
         { text: "Name", value: "{{patient_name}}" },
         { text: "Telephone", value: "{{telephone}}" },
         { text: "Mobile", value: "{{mobile}}" },
         { text: "Email", value: "{{email}}" },
         { text: "Clinic", value: "{{clinic_name}}" },
         { text: "Promoter", value: "{{promoter_name}}" },
         { text: "Date", value: "{{date_format}}" },
         { text: "Hour", value: "{{time_format}}" }
     ]
    });
});