from django import template
from remailer.models import Promoter, Clinic

register = template.Library()


@register.inclusion_tag('tags/items_list.html')
def promotor_list():
    return {
        'list': Clinic.objects.all()
    }


@register.inclusion_tag('tags/items_list.html')
def clinic_list():
    return {
        'list': Promoter.objects.all()
    }
