from django import template
from remailer.models import Patient

register = template.Library()


@register.inclusion_tag('tags/patient_list.html', takes_context=True)
def weather(context):
    request = context['request']
    patients = Patient.objects.all()

    if request.method == 'GET':
        order = request.GET.get('order_by', '')
        if order is not None and order != '':
            patients = patients.order_by('%s' % order)
    return {
        'patients': patients
    }
