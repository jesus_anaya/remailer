from django.conf.urls import patterns, url
from django.views.generic.simple import redirect_to


urlpatterns = patterns(
    'remailer.views',
    # Examples:
    url(r'^$', redirect_to, {'url': '/remailer/patient/add/'}),
    url(r'^remailer/patient/$', redirect_to, {'url': '/remailer/patient/add/'}),
    url(r'^delete_patient/(?P<id>\d+)/$', "delete_patient"),
    url(r'^patient-validation/$', "patient_validation"),
    url(r'^change_date_patient/(?P<id>\d+)/$', "change_date_patient"),
    url(r'^change_to_no_show/(?P<id>\d+)/$', "change_to_no_show"),
    url(r'^export/$', "export"),
    url(r'^exportscv/$', "get_scv", name="export_scv"),
)
