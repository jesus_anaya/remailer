#encoding:utf-8

from django import forms
from models import Patient, Email


class FormattedDateField(forms.DateField):
    widget = forms.DateInput(format='%m-%d-%Y')

    def __init__(self, *args, **kwargs):
        super(FormattedDateField, self).__init__(*args, **kwargs)
        self.input_formats = ['%m-%d-%Y', '%Y-%m-%d'] + list(self.input_formats)


class PatientForm(forms.ModelForm):
    class Meta:
        model = Patient

    date_data = FormattedDateField(required=False)
    contact_date = FormattedDateField(required=False)
    reward_name = forms.CharField(required=False)
    promo_code = forms.CharField(required=False)
    city_of_residence = forms.CharField(required=False)
    state = forms.IntegerField(required=False)
    birth_date = forms.DateField(required=False)
    city_of_residence = forms.CharField(required=False)
    promo_code = forms.CharField(required=False)


class EmailForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'cols': 60, 'rows': 15}))

    class Meta:
        model = Email
