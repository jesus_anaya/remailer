from django.contrib import admin
#from django.contrib.admin import helpers
from models import Patient, Clinic, Email, Promoter, BCCMail
from remailer.forms import PatientForm, EmailForm
from django import forms


class PatientAdmin(admin.ModelAdmin):
    form = PatientForm
    fields = (
        (
            'contact_date', 'first_name', 'last_name', 'time_data', 'birth_date', 'promo_code',
            'date_data', 'date', 'email', 'telephone', 'mobile', 'city_of_residence',
            'treatment', 'clinic', 'promoter', 'comment', 'reward_program', 'reward_name', 'procedures'
        ),
    )

    list_display = ('first_name', 'last_name', 'date', 'email', 'telephone', 'mobile', 'clinic', 'treatment')

    def save_model(self, request, obj, form, change):
        print "save_model"
        print "Current user is: ", request.user
        obj.user = request.user

        if request.method == 'POST':
            if request.POST.get('_udate_appointment'):
                obj.email_phase = 0

        obj.save()


class ClinicForm(forms.ModelForm):
    class Meta:
        model = Clinic

    #email_confirm_password = forms.CharField(widget=forms.PasswordInput())
    #email_remember_password = forms.CharField(widget=forms.PasswordInput())
    #email_feedback_password = forms.CharField(widget=forms.PasswordInput())


class ClinicAdmin(admin.ModelAdmin):
    form = ClinicForm

    fieldsets = (
        ('Datos de la clinica', {
            'fields': ('name', 'address', 'telephone')
        }),
        ('Correos', {
            'fields': (
                ('email_confirm', 'email_confirm_password', 'email_confirm_server', 'email_confirm_port'),
                ('email_remember', 'email_remember_password', 'email_remember_server', 'email_remember_port'),
                ('email_feedback', 'email_feedback_password', 'email_feedback_server', 'email_feedback_port'),
                ('email_sanico', 'email_sanico_password', 'email_sanico_server', 'email_sanico_port')
            )
        }),
    )


class EmailAdmin(admin.ModelAdmin):
    form = EmailForm

    class Media:
        js = (
            "/static/arkiadmin/js/kendo/kendo.all.min.js",
            "/static/js/email_script.js"
        )

        css = {
            'all': (
                '/static/arkiadmin/css/kendo/kendo.common.min.css',
                '/static/arkiadmin/css/kendo/kendo.default.min.css',
                '/static/css/email_style.css',
            )
        }


class PromoterAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')


admin.site.register(Patient, PatientAdmin)
admin.site.register(Clinic, ClinicAdmin)
admin.site.register(Email, EmailAdmin)
admin.site.register(Promoter, PromoterAdmin)
admin.site.register(BCCMail)
