### Jesus Anaya
###
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import Charset
from email.header import Header
from cStringIO import StringIO
from email.generator import Generator
from django.core.paginator import Paginator, InvalidPage, EmptyPage


def send_mail_gapps(message, subject, user, pwd, to, server, port, bcc=[]):
    Charset.add_charset('utf-8', Charset.QP, Charset.QP, 'utf-8')

    msg = MIMEMultipart('alternative')
    msg['From'] = user
    msg['To'] = to
    msg['Cc'] = ''
    msg['Subject'] = Header(unicode(subject), 'utf-8')
    msg.attach(MIMEText(message, 'html', 'UTF-8'))

    str_io = StringIO()
    g = Generator(str_io, False)
    g.flatten(msg)

    mailServer = smtplib.SMTP_SSL(server, int(port), timeout=10)
    mailServer.set_debuglevel(0)

    try:
        mailServer.login(user, pwd)

        toaddrs = [to] + bcc
        mailServer.sendmail(user, toaddrs, str_io.getvalue())
        mailServer.close()
    except Exception, error:
        print "Unable to send e-mail: '%s'." % str(error)
        mailServer.quit()


def to_tuple(x):
    return zip(x, x)


def paginator(request, queryset, pag_num):
    p = Paginator(queryset, pag_num)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        fp = p.page(page)
    except (EmptyPage, InvalidPage):
        fp = p.page(p.num_pages)

    return fp
