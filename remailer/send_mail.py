#from django.core.mail import EmailMultiAlternatives
from helpers import send_mail_gapps
from django.utils.html import strip_tags
from django.template import Template, Context
from remailer.models import Email, Clinic
from remailer.models.bcc_mail import BCCMail
import sys
import time


def send_mail(patient, bcc=[]):
    emails = Email.objects.filter(
        clinic=patient.clinic).filter(emaile_type=patient.email_phase)

    try:
        email = list(emails)[0]
    except Exception, error:
        print "Unable get template: '%s'." % str(error)
        return False

    try:
        mail_context = Context({
            "patient_name": patient.patient_name,
            "telephone": patient.telephone,
            "get_datetime": patient.get_datetime,
            "mobile": patient.mobile,
            "email": patient.email,
            "clinic_name": patient.clinic_name,
            "promoter_name": patient.promoter_name,
            "date_data": patient.date_data,
            "time_data": patient.time_data,
            "time_format": patient.time_format(),
            "date_format": patient.date_format()
        })
        mail_text = Template(email.body).render(mail_context)
        subject_text = Template(email.subject).render(mail_context)

        for bcc_email in BCCMail.objects.filter(clinic=patient.clinic.id):
            bcc += [bcc_email.email]

        from_email = ""
        clinic = Clinic.objects.get(id=patient.clinic.id)
        if patient.email_phase == 0:
            from_email = clinic.email_confirm
            from_password = clinic.email_confirm_password
            from_server = clinic.email_confirm_server
            from_port = clinic.email_confirm_port
        elif patient.email_phase == 1:
            from_email = clinic.email_remember
            from_password = clinic.email_remember_password
            from_server = clinic.email_remember_server
            from_port = clinic.email_remember_port
        elif patient.email_phase == 2:
            from_email = clinic.email_feedback
            from_password = clinic.email_feedback_password
            from_server = clinic.email_feedback_server
            from_port = clinic.email_feedback_port
        elif patient.email_phase == 3:
            from_email = clinic.email_sanico
            from_password = clinic.email_sanico_password
            from_server = clinic.email_sanico_server
            from_port = clinic.email_sanico_port

        #to send mail
        #to = [patient.email]

        #text_content = strip_tags(mail_text)
        #msg = EmailMultiAlternatives(subject_text, text_content, from_email, to, bcc=bcc)
        #msg.attach_alternative(mail_text, "text/html")
        send_mail_gapps(
            message=mail_text,
            subject=subject_text,
            user=from_email,
            pwd=from_password,
            to=patient.email,
            server=from_server,
            port=from_port,
            bcc=bcc
        )
        #msg.send()

        #sleep a second betwen a email
        time.sleep(6)

    except:
        print "Error send mail | Unexpected error:", sys.exc_info()
        return False

    return True
