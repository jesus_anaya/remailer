#encoding:utf-8

from backbone.views import BackboneAPIView
from remailer.models import Patient, Clinic, Promoter
from remailer.forms import PatientForm
import backbone


class PatientAPIView(BackboneAPIView):
    model = Patient
    form = PatientForm
    display_fields = (
        'id', 'first_name', 'last_name', 'time_data', 'date_data', 'email',
        'telephone', 'mobile', 'clinic', 'treatment', 'comment', 'patient_info',
        'promoter', 'date', 'user', 'clinic_name', 'contact_date_filter',
        'user_name', 'promoter_name', 'date_converted', 'contact_date_format'
    )

    fields = (
        'clinic', 'comment', 'date', 'time_data', 'email', 'first_name',
        'last_name', 'mobile', 'promoter', 'telephone', 'treatment', 'user',
        'date_data', 'contact_date'
    )


class ClinicAPIView(BackboneAPIView):
    model = Clinic
    display_fields = ('id', 'name', 'address', 'telephone')


class PromoterAPIView(BackboneAPIView):
    model = Promoter
    display_fields = ('id', 'name')


backbone.site.register(PatientAPIView)
backbone.site.register(ClinicAPIView)
backbone.site.register(PromoterAPIView)
