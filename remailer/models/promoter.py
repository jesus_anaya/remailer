#encoding:utf-8
from django.db import models


class Promoter(models.Model):
    class Meta:
        ordering = ['name']
        verbose_name = "Promoter"
        app_label = "remailer"

    name = models.CharField(max_length=60, verbose_name="Name")
    email = models.EmailField(blank=True, null=True)

    def __unicode__(self):
        return unicode(self.name)
