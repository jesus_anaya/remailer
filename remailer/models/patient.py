#encoding:utf-8
from django.db import models
from django.contrib.auth.models import User
from django.db.models import signals
from clinic import Clinic
from promoter import Promoter
from remailer.send_mail import send_mail
from datetime import datetime, timedelta
import sys
import re


class Patient(models.Model):
    class Meta:
        verbose_name = "Patient"
        app_label = "remailer"
        ordering = ['-contact_date', '-id']

    first_name = models.CharField(max_length=50, verbose_name="First name")
    last_name = models.CharField(max_length=50, verbose_name="Last name")
    contact_date = models.DateField(null=True, verbose_name="Contact Date")
    time_data = models.TimeField(blank=True, null=True, verbose_name="Hour")
    date_data = models.DateField(blank=True, null=True, verbose_name="Date")
    email = models.EmailField(blank=True, null=True, max_length=100, verbose_name="E-Mail")
    telephone = models.CharField(max_length=25, verbose_name="Telephone")
    mobile = models.CharField(blank=True, null=True, max_length=25, verbose_name="Mobile")
    clinic = models.ForeignKey(Clinic, verbose_name="Clinic")
    treatment = models.CharField(blank=True, null=True, max_length=100, verbose_name="Appointment Status")
    comment = models.TextField(blank=True, null=True, verbose_name="Comment")
    promoter = models.ForeignKey(Promoter, blank=True, null=True, verbose_name="Promoter")
    date = models.BooleanField(default=False, verbose_name="Active Date")
    state = models.IntegerField(default=0)
    user = models.ForeignKey(User, blank=True, null=True)
    email_phase = models.IntegerField(default=0, blank=True, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    search_info = models.CharField(max_length=200, blank=True, null=True)
    birth_date = models.DateField(null=True, blank=True)
    city_of_residence = models.CharField(max_length=100, default="")
    promo_code = models.CharField(max_length=50, default="")

    #Sani Reward Program Data
    reward_program = models.BooleanField(default=False)
    reward_name = models.CharField(max_length=120, default="")

    #procedures
    procedures = models.TextField(default='')

    def __unicode__(self):
        return unicode("%s %s" % (self.first_name, self.last_name))

    def resent_hour(self):
        try:
            return datetime.now() - timedelta(hours=1) < self.modify_date
        except:
            return False

    def resent_day(self):
        try:
            return datetime.now() - timedelta(days=1) < self.modify_date
        except:
            return False

    def patient_name(self):
        return unicode("%s %s" % (self.first_name, self.last_name))

    def patient_info(self):
        return "%s %s %s" % (self.first_name, self.last_name, self.email)

    def get_datetime(self):
        return "%s %s" % (str(self.date_data), str(self.time_data))

    def clinic_name(self):
        try:
            return self.clinic.name
        except:
            return ""

    def user_name(self):
        try:
            return self.user.first_name
        except:
            return ""

    def promoter_name(self):
        try:
            return self.promoter.name
        except:
            return ""

    def time_format(self):
        if self.time_data:
            return self.time_data
        return ''

    def contact_date_format(self):
        if self.contact_date:
            return self.contact_date.strftime('%m-%d-%Y')
        return ''

    def contact_date_filter(self):
        if self.contact_date:
            return self.contact_date.strftime('%Y%m%d')
        return ''

    def date_format(self):
        try:
            return self.date_data.strftime('%b %d, %Y')
        except:
            return ''

    def date_converted(self):
        if self.date_data:
            return self.date_data.strftime('%m-%d-%Y')
        return ''

    def change_to_no_show(self):
        self.date = False
        self.state = 3
        self.save()
        send_reschedule_email(self)

    def save(self, *args, **kwargs):
        self.search_info = re.sub(
            r'\s+', ' ', "%s %s %s" % (self.first_name, self.last_name, self.email))

        if self.state < 2:
            self.state = int(self.date)

        if not self.email_phase:
            self.email_phase = 0
        #send confirmation mail
        super(Patient, self).save(*args, **kwargs)


def find_procedure(obj, procedures):
    return [x for x in procedures if str(obj.procedures).find(x) != -1]


def send_confirmation_mail(sender, **kwargs):
    obj = kwargs['instance']

    procedures_list = (
        'Implant Crown',
        'Implant Crown (other Brands)',
        'Snap-on Final Plate',
        'All on 4/6/8/ Final Plate',
        'Prettau Final Plate'
    )

    if obj.date:
        if obj.email_phase == 0 or obj.state == 3:
            try:
                senders = []
                if str(obj.treatment) == "Warranty" or str(obj.procedures).find('All-on 4') != -1:
                    print "Send Warranty"
                    senders.append('sanilab@sanidentalgroup.com')

                if find_procedure(obj, procedures_list):
                    print "Send copy to ninadelgadosdg@gmail.com"
                    senders.append('ninadelgadosdg@gmail.com')

                if str(obj.procedures).find('All-on 4') != -1:
                    senders.append('instrumental@sanidentalgroup.com')

                if obj.promoter.email:
                    print "Send to promoter: {0}".format(obj.promoter)
                    senders.append(str(obj.promoter.email))

                send_mail(obj, senders)
            except:
                print "error:", sys.exc_info()
            obj.email_phase = 1
            obj.save()


def send_reschedule_email(obj):
    try:
        senders = []
        if str(obj.treatment) == "Warranty" or str(obj.procedures).find('All-on 4') != -1:
            print "Send Warranty"
            senders.append('sanilab@sanidentalgroup.com')

        if str(obj.procedures).find('All-on 4') != -1:
            senders.append('instrumental@sanidentalgroup.com')

        if obj.promoter.email:
            print "Send to promoter: {0}".format(obj.promoter)
            senders.append(str(obj.promoter.email))

        send_mail(obj, senders)
    except:
        print "error:", sys.exc_info()
    obj.email_phase = 1
    obj.save()

signals.post_save.connect(send_confirmation_mail, sender=Patient)
