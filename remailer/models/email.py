#encoding:utf-8
#
#
from django.db import models
from clinic import Clinic


class Email(models.Model):
    class Meta:
        verbose_name = "Email"
        app_label = "remailer"
        ordering = ['-id']

    TYPE = (
        (0, "Confirmation"),
        (1, "Reminder"),
        (2, "Feedback"),
        (3, "Sanico")
    )

    clinic = models.ForeignKey(Clinic, verbose_name="Clinic")
    emaile_type = models.IntegerField(choices=TYPE,
        verbose_name="Email type")
    subject = models.CharField(max_length=150, verbose_name="Subject")
    body = models.TextField(verbose_name="Body")

    def __unicode__(self):
        return unicode("%s %s" % (self.clinic.name, self.TYPE[self.emaile_type][1]))
