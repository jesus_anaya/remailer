#encoding:utf-8
from django.db import models
from clinic import Clinic


class BCCMail(models.Model):
    class Meta:
        verbose_name = "BCC Mail"
        app_label = "remailer"

    email = models.EmailField(max_length=100, verbose_name="E-Mail")
    clinic = models.ForeignKey(Clinic, verbose_name="Clinic")

    def __unicode__(self):
        return unicode("%s - %s" % (self.clinic.name, self.email))
