#encoding:utf-8
from django.db import models


class Clinic(models.Model):
    class Meta:
        verbose_name = "Clinic"
        app_label = "remailer"

    name = models.CharField(max_length=150, verbose_name="Name")
    address = models.CharField(max_length=200, verbose_name="Address")
    telephone = models.CharField(max_length=25, verbose_name="Telephone")

    # Confirmation Email
    email_confirm = models.EmailField(verbose_name="Confirmation")
    email_confirm_password = models.CharField(max_length=50, verbose_name="Password")
    email_confirm_server = models.CharField(max_length=100, verbose_name="Server")
    email_confirm_port = models.IntegerField(verbose_name="Port", default=465)

    # Remember Email
    email_remember = models.EmailField(verbose_name="Reminder")
    email_remember_password = models.CharField(max_length=50, verbose_name="Password")
    email_remember_server = models.CharField(max_length=100, verbose_name="Server")
    email_remember_port = models.IntegerField(verbose_name="Port", default=465)

    # Feedback Email
    email_feedback = models.EmailField(verbose_name="Feedback")
    email_feedback_password = models.CharField(max_length=50, verbose_name="Password")
    email_feedback_server = models.CharField(max_length=100, verbose_name="Server")
    email_feedback_port = models.IntegerField(verbose_name="Port", default=465)

    # Feedback Email
    email_sanico = models.EmailField(verbose_name="Sanico", null=True)
    email_sanico_password = models.CharField(max_length=50, verbose_name="Password", null=True)
    email_sanico_server = models.CharField(max_length=100, verbose_name="Server", null=True)
    email_sanico_port = models.IntegerField(verbose_name="Port", default=465)

    def __unicode__(self):
        return unicode(self.name)
