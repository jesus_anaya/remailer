# -*- encoding: utf-8 -*-
from django.core.management.base import NoArgsCommand
from remailer.models import Patient
from remailer.send_mail import send_mail


class Command(NoArgsCommand):
    help = "Test para probar el envio automatico de correos"

    def handle_noargs(self, **options):
        patients = Patient.objects.filter(date=True)

        for patient in list(patients):
            if patient.email_phase == 1:
                self.send_premail(patient)
                print "email send email_phase 1"
            elif patient.email_phase == 2:
                self.send_postmail(patient)
                print "email send email_phase 2"
    print "no email send"

    def send_premail(self, patient):
        send_mail(patient)
        patient.email_phase = 2
        patient.save()

    def send_postmail(self, patient):
        send_mail(patient)
        patient.email_phase = 3
        patient.save()
