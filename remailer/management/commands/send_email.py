# -*- encoding: utf-8 -*-
from django.core.management.base import NoArgsCommand
from remailer.models import Patient
from remailer.send_mail import send_mail
from datetime import date
import datetime
import sys


class Command(NoArgsCommand):
    help = "Comando para automatizar el envio de correos"

    def handle_noargs(self, **options):
        self.emails_counter = 0
        try:
            patients = Patient.objects.filter(date=True)

            for patient in list(patients):
                print "Processing: ", patient

                #limit the email send to 200 per 30 minutes
                if self.emails_counter == 200:
                    break

                # send reminder email
                if patient.email_phase == 1:
                    try:
                        if date.today() >= patient.date_data - datetime.timedelta(days=3)\
                        and date.today() <= patient.date_data + datetime.timedelta(days=1):
                            self.send_premail(patient)
                            print "email send reminder to: %s %s" % (str(patient), str(patient.email))
                        else:
                            patient.email_phase = 2
                            patient.save()
                            print "email send reminder expired, waiting for feedback to: %s %s" %\
                                (str(patient), str(patient.email))
                    except:
                        print "Error sending remainder to", patient

                # send feedback email
                elif patient.email_phase == 2:
                    try:
                        if date.today() >= patient.date_data + datetime.timedelta(days=3):
                            self.send_postmail(patient)
                            print "email send feedback to: %s %s" % (str(patient), str(patient.email))
                    except:
                        print "Error sending feedback to", patient
        except:
            print "Unexpected error:", sys.exc_info()[0]


    def send_premail(self, patient):
        if send_mail(patient):
            self.emails_counter += 1
            patient.email_phase = 2
            patient.save()


    def send_postmail(self, patient):
        if send_mail(patient):
            self.emails_counter += 1
            patient.email_phase = 3
            patient.save()


    def send_sanicomail(self, patient):
        if send_mail(patient):
            self.emails_counter += 1
            patient.email_phase = 4
            patient.save()

